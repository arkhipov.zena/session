﻿using System;
using System.IO;

namespace ПовторСессия
{
    public static class WorkWithData
    {
        public static string[] DataToString(string fileName)
        {
            string path = Path.Combine(Environment.CurrentDirectory, fileName);
            string[] data = File.ReadAllLines(path);
            return data;
        }
        public static Figure StringToFigure(string data)
        {
            string[] dataSplit = data.Split(' ');
            return new Figure(  
                dataSplit[0],
                dataSplit[1],
                dataSplit[2],
                dataSplit[3]
                );
        }
    }
    public class Figure
    {
        public string Color { get; set; }
        public string Type { get; set; }
        public string BeginPos { get; set; }
        public string EndPos { get; set; }
        public Figure(string Color, string type, string beginPos, string endPos)
        {
            this.Color = Color;
            this.Type = type;
            this.BeginPos = beginPos;
            this.EndPos = endPos;
        }
    }
    public static class Chess
    {
        public static void CheckAllFigure(string fileName)
        {
            string[] data = WorkWithData.DataToString(fileName);
            foreach (var dataString in data)
            {
                Figure figure = WorkWithData.StringToFigure(dataString);
                ChessCheck.CheckFigureColor(figure);
            }
        }
    }
    public static class ChessCheck
    {
        public static void CheckFigureColor(Figure figure)
        {
            if (figure.Color == "black" || figure.Color == "white")
                CheckFigureType(figure);
            else
                Console.WriteLine("Неправильно введен цвет");
        }
        public static void CheckFigureType(Figure figure)
        {
            char[] beginPos = new char[] { figure.BeginPos[0], figure.BeginPos[1] };
            char[] endPos = new char[] { figure.EndPos[0], figure.EndPos[1] };
            if (beginPos[0] <= 'h' && beginPos[0] >= 'a' && endPos[0] <= 'h' && endPos[0] >= 'a' && beginPos[1] >= '1' && beginPos[1] <= '8' && endPos[1] >= '1' && endPos[1] <= '8')
                switch (figure.Type)
                {
                    case "pawn":
                        CheckFigurePawn(beginPos, endPos);
                        break;
                    case "knight":
                        CheckFigureKnight(beginPos, endPos);
                        break;
                    case "bishop":
                        CheckFigureBishop(beginPos, endPos);
                        break;
                    case "rook":
                        CheckFigureRook(beginPos, endPos);
                        break;
                    case "king":
                        CheckFigureKing(beginPos, endPos);
                        break;
                    case "queen":
                        CheckFigureQueen(beginPos, endPos);
                        break;
                    default:
                        Console.WriteLine("Неправильно введен тип фигуры");
                        break;
                }
            else
                Console.WriteLine("Некоректный ввод клетки");
        }
        public static void CheckFigurePawn(char[] beginPos, char[] endPos)
        {
            if ((beginPos[1] == '2' || beginPos[1] == '7') && beginPos[0] == endPos[0] && Math.Abs(beginPos[1] - endPos[1]) <= 2)
                Console.WriteLine("Эта фигура (пешка) ходит коректно");
            else
                if (beginPos[0] == endPos[0] && Math.Abs(beginPos[1] - endPos[1]) == 1)
                    Console.WriteLine("Эта фигура (пешка) ходит коректно");
                else
                    Console.WriteLine("Пешка так не ходит");
        }
        public static void CheckFigureKnight(char[] beginPos, char[] endPos)
        {
            if ((Math.Abs(beginPos[1] - endPos[1]) == 1 && Math.Abs(beginPos[0] - endPos[0]) == 2) || (Math.Abs(beginPos[1] - endPos[1]) == 2 && Math.Abs(beginPos[0] - endPos[0]) == 1))
                Console.WriteLine("Эта фигура (пешка) ходит коректно");
            else
                Console.WriteLine("Пешка так не ходит");

        }
        public static void CheckFigureBishop(char[] beginPos, char[] endPos)
        {
            if (Math.Abs(beginPos[1] - endPos[1]) == Math.Abs(beginPos[0] - endPos[0]))
                Console.WriteLine("Эта фигура (слон) ходит коректно");
            else
                Console.WriteLine("Слон так не ходит");
        }
        public static void CheckFigureRook(char[] beginPos, char[] endPos)
        {
            if (beginPos[0] == endPos[0] || beginPos[1] == endPos[1])
                Console.WriteLine("Эта фигура (ладья) ходит коректно");
            else
                Console.WriteLine("Ладья так не ходит");
        }
        public static void CheckFigureKing(char[] beginPos, char[] endPos)
        {
            if (Math.Abs(beginPos[1] - endPos[1]) <= 1 && Math.Abs(beginPos[0] - endPos[0]) <= 1)
                Console.WriteLine("Эта фигура (король) ходит коректно");
            else
                Console.WriteLine("Король так не ходит");
        }
        public static void CheckFigureQueen(char[] beginPos, char[] endPos)
        {
            if ((beginPos[0] == endPos[0] || beginPos[1] == endPos[1]) || (Math.Abs(beginPos[1] - endPos[1]) == Math.Abs(beginPos[0] - endPos[0])))
                Console.WriteLine("Эта фигура (королева) ходит коректно");
            else
                Console.WriteLine("Король так не ходит");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Chess.CheckAllFigure("figure.txt");
        }
    }
}